import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './layouts/default/default.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { CoursesComponent } from './modules/courses/courses.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AuthDefaultComponent } from './layouts/auth-default/auth-default.component';
import { CourseShowComponent } from './modules/courses/course-show/course-show.component';
import { CertificateComponent } from './modules/courses/certificate/certificate.component';
import { CourseCreateComponent } from './modules/courses/course-create/course-create.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: DefaultComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'courses',
        component: CoursesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'course/new',
        component: CourseCreateComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'course/show/:id',
        component: CourseShowComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'course/certificate',
        component: CertificateComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
