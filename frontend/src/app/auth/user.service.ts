import { Injectable } from '@angular/core';
import { User } from './user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http: HttpClient) { }

  public user:User = { username: "", email: "", password: "" };

  public isadmin:boolean = true;

  public login(user:User):Observable<User> {
    return this._http.post<User>("api/user/login", user);
  }

  public signup(user:User):Observable<User> {
    return this._http.post<User>("api/user/signup", user);
  }

  public logout() {
  	localStorage.clear();
  }
}
