import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Router } from '@angular/router';

declare var require: any;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  constructor(private _us:UserService, private _router: Router) { }

  public logo = require("src/app/auth/Indium-software-Logo.png");

  public user:User = { username: "", email: "", password: "" };

  ngOnInit(): void {
  }

  signup(form) {
    this._us.signup(form.value).subscribe(
      res => {
        this._router.navigate(["/login"]);
      }
    )
  }

}
