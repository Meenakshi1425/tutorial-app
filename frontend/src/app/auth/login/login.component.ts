import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Router } from '@angular/router';

declare var require: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private _us:UserService, private _router: Router) { }

  public logo = require("src/app/auth/Indium-software-Logo.png");

  public user:User = { username: "", email: "", password: "" };

  ngOnInit(): void {
  }

  login(form) {
    this._us.login(form.value).subscribe(
      res => {
        let length = Object.keys(res).length;
  			if(length == 5) {
         localStorage.setItem("currentuser", res.username);
         localStorage.setItem("email", this.user.email);
  			 this._router.navigate(["/"]);
  			}
  			else {
  			 this._router.navigate(["/login"]);
  			}
  		},
  		err => {}
    )
  }

}
