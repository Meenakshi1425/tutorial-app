import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Course } from './course';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private _http: HttpClient) { }

  public upload_course(course):Observable<Course> {
    console.log("course", course);
    return this._http.post<Course>("api/course/upload", course);
  }

  public get_all_courses(pagesize:number, page:number):Observable<Course[]> {
    const query = `?pagesize=${pagesize}&page=${page}`;
    return this._http.get<Course[]>("api/course/videos" + query);
  }

  public get_course(id:string):Observable<Course> {
    return this._http.get<Course>("api/course/video/" + id);
  }
}
