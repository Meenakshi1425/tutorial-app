import { Component, OnInit } from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';

declare var require: any;

@Component({
  selector: 'app-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.css']
})
export class CertificateComponent implements OnInit {

  constructor() { }

  public logo = require("src/app/modules/courses/Indium-software-Logo.png");

  ngOnInit(): void {
  }

  certificate() {
    var element = document.getElementById('certificate');
    html2canvas(element, { allowTaint: false, useCORS: true }).then((canvas) => {
      var imgdata = canvas.toDataURL('image/png');
      console.log("canvas", canvas);
      var doc = new jspdf();
      var imgHeight = canvas.height * 380 / canvas.width;
      doc.addImage(imgdata, 0, 0, 210, imgHeight);
      doc.save('certificate.pdf');
    })
  }
}
